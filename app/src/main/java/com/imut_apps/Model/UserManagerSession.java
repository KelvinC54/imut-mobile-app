package com.imut_apps.Model;

import android.content.SharedPreferences;

import com.imut_apps.Response.UserResponse;

public class UserManagerSession {


    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static UserManagerSession INSTANCE = null;

    private UserManagerSession(SharedPreferences prefs) {
        this.prefs = prefs;
        this.editor = prefs.edit();
    }

    public static synchronized UserManagerSession getInstance(SharedPreferences prefs){
        if(INSTANCE == null){
            INSTANCE = new UserManagerSession(prefs);
        }
        return INSTANCE;
    }

    public void saveUser(UserResponse user) {
        editor.putString("ID", String.valueOf(user.getId())).commit();
        editor.putString("NAME", user.getName()).commit();
        editor.putString("EMAIL", user.getEmail()).commit();
        editor.putString("ALAMAT", user.getAlamat()).commit();
        editor.putString("NOTELP", user.getNotelp()).commit();
        editor.putString("FOTO", user.getFoto()).commit();
//            editor.putString("REFRESH_TOKEN", token.getRefreshToken()).commit();
    }

    public void updateUser(String name, String email, String alamat, String notelp, String foto){
        editor.putString("NAME", name).commit();
        editor.putString("EMAIL", email).commit();
        editor.putString("ALAMAT", alamat).commit();
        editor.putString("NOTELP", notelp).commit();
        editor.putString("FOTO", foto).commit();
    }

    public void deleteUser() {
        editor.remove("ID").commit();
        editor.remove("NAME").commit();
        editor.remove("EMAIL").commit();
        editor.remove("ALAMAT").commit();
        editor.remove("NOTELP").commit();
        editor.remove("FOTO").commit();

//            editor.remove("REFRESH_TOKEN").commit();
    }

    public UserResponse getUser() {
        UserResponse user = new UserResponse();
        user.setId(Integer.parseInt(prefs.getString("ID", null)));
        user.setName(prefs.getString("NAME", null));
        user.setEmail(prefs.getString("EMAIL", null));
        user.setAlamat(prefs.getString("ALAMAT", null));
        user.setNotelp(prefs.getString("NOTELP", null));
        user.setFoto(prefs.getString("FOTO", null));
//            token.setRefreshToken(prefs.getString("REFRESH_TOKEN", null));
        return user;
    }


}
