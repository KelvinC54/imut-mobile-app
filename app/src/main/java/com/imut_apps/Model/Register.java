package com.imut_apps.Model;

public class Register {
    private String name,email,password,password_confirmation;

    public Register(String name, String email, String password, String password_confirmation) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.password_confirmation = password_confirmation;
    }
}
