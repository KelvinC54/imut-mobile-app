package com.imut_apps.Model;

import android.content.SharedPreferences;

import com.imut_apps.Response.LoginResponse;

public class TokenManager {


    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static TokenManager INSTANCE = null;

    private TokenManager(SharedPreferences prefs) {
        this.prefs = prefs;
        this.editor = prefs.edit();
    }

    public static synchronized TokenManager getInstance(SharedPreferences prefs){
        if(INSTANCE == null){
            INSTANCE = new TokenManager(prefs);
        }
        return INSTANCE;
    }

    public void saveToken(LoginResponse token) {
        editor.putString("ACCESS_TOKEN", token.getAccess_token()).commit();
//            editor.putString("REFRESH_TOKEN", token.getRefreshToken()).commit();
    }

    public void deleteToken() {
        editor.remove("ACCESS_TOKEN").commit();
//            editor.remove("REFRESH_TOKEN").commit();
    }

    public LoginResponse getToken() {
        LoginResponse token = new LoginResponse();
        token.setAccess_token(prefs.getString("ACCESS_TOKEN", null));
//            token.setRefreshToken(prefs.getString("REFRESH_TOKEN", null));
        return token;
    }


}
