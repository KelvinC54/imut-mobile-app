package com.imut_apps.Api;

import com.imut_apps.Model.Login;
import com.imut_apps.Model.Register;
import com.imut_apps.Response.LoginResponse;
import com.imut_apps.Response.MessageResponse;
import com.imut_apps.Response.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @POST("api/auth/login")
    Call<LoginResponse> login(@Body Login login);

    @GET("api/auth/user")
    Call<UserResponse> getSecret(@Header("Authorization") String authToken);


    @POST("api/auth/signup")
    Call<MessageResponse> register(@Body Register register);


    @GET("api/auth/logout")
    Call<MessageResponse> logout(@Header("Authorization") String authToken);

    @POST("api/update_user/{id}")
    @FormUrlEncoded
    Call<MessageResponse> updateUser(
            @Path("id") int id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("notelp") String notelp,
            @Field("alamat") String alamat
    );
}
