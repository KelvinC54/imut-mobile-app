package com.imut_apps.User;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.imut_apps.Network;
import com.imut_apps.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private EditText username, email, password, confirm_password;
    private Button btn_regist;
    private TextView tv_login;
    Intent intent;
    int success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = findViewById(R.id.usernameRegister);
        email = findViewById(R.id.emailRegister);
        password = findViewById(R.id.passwordRegister);
        confirm_password = findViewById(R.id.confirmPasswordRegister);
        btn_regist = findViewById(R.id.btnRegister);
        tv_login = findViewById(R.id.tvLogin);

        tv_login.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                intent = new Intent(RegisterActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        });

        btn_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Regist();

            }
        });
    }

    public void Regist(){
        btn_regist.setVisibility(View.GONE);
        final String username = this.username.getText().toString();
        final String email = this.email.getText().toString();
        final String password = this.password.getText().toString();

        if (username.trim().length() > 0 && email.trim().length() > 0 && password.trim().length() > 0){
            if (confirm_password.equals(password)){
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    success = jsonObject.getInt("success");
                                    if (success == 1) {
                                        Toast.makeText(RegisterActivity.this, "Register Success", Toast.LENGTH_SHORT).show();
                                        intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                        finish();
                                        startActivity(intent);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(RegisterActivity.this, "Register Error" + e.toString(), Toast.LENGTH_SHORT).show();
                                    btn_regist.setVisibility(View.VISIBLE);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(RegisterActivity.this, "Register Error" + error.toString(), Toast.LENGTH_SHORT).show();
                                btn_regist.setVisibility(View.VISIBLE);
                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<>();
                        params.put("username", username);
                        params.put("email", email);
                        params.put("password", password);
                        params.put("confirm_password", confirm_password);
                        return params;
                    }
                };
                Volley.newRequestQueue(this).add(stringRequest);
            }else{
                Toast.makeText(getApplicationContext(),"Password tidak cocok",Toast.LENGTH_LONG).show();
                btn_regist.setVisibility(View.VISIBLE);
            }
        }else{
            Toast.makeText(getApplicationContext(), "Kolom tidak boleh kosong", Toast.LENGTH_LONG).show();
            btn_regist.setVisibility(View.VISIBLE);
        }
    }
}