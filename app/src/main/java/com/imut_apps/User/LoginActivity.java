package com.imut_apps.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.imut_apps.Api.ApiInterface;
import com.imut_apps.Api.ApiRequest;
import com.imut_apps.Activity.MainActivity;
import com.imut_apps.Model.Login;
import com.imut_apps.Model.TokenManager;
import com.imut_apps.R;
import com.imut_apps.Response.LoginResponse;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText email,password;
    ApiInterface userClient = ApiRequest.getRetrofit().create(ApiInterface.class);
    public String token;
    TokenManager tokenManager;
    TextView btnRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Typeface bold=Typeface.createFromAsset(getAssets(),"fonts/NeoSansStd-Bold.otf");
        Typeface regular=Typeface.createFromAsset(getAssets(), "fonts/NeoSansStd-Regular.otf");
        email = findViewById(R.id.emailLogin);
        password = findViewById(R.id.passwordLogin);

        email.setTypeface(regular);
//        Typeface customfont3=Typeface.createFromAsset(getAssets(), "fonts/NeoSansStd-Regular.otf");
        password.setTypeface(regular);


        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));

        Button btnlogin = findViewById(R.id.btnLogin);

        btnlogin.setTypeface(bold);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
//
        if(tokenManager.getToken().getAccess_token() != null){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setTypeface(bold);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }




    private void login(){
        final String userEmail = email.getText().toString().trim();
        final String userPassword = password.getText().toString().trim();

        if (TextUtils.isEmpty(userEmail)){
            email.setError("Email tidak boleh kosong");
            email.requestFocus();
        }else if (TextUtils.isEmpty(userPassword)){
            password.setError("Password tidak boleh kosong");
            password.requestFocus();
        } else{
            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Login...");
            progressDialog.show();

            Login login = new Login(userEmail, userPassword, true);
            Call<LoginResponse> call = userClient.login(login);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.isSuccessful()){
                        tokenManager.saveToken(response.body());
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, "Login Sukses!", Toast.LENGTH_SHORT).show();
                        startMainActivity();
                        finish();

                    } else{
                        Toast.makeText(LoginActivity.this, "Login Gagal!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Error!", Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
                }
            });
        }

        finish();

    }

    public void startMainActivity(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void register(){
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

}
